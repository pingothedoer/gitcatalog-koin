package com.pingo.gitcatalog

import androidx.room.Room
import androidx.test.InstrumentationRegistry
import com.pingo.gitcatalog.app.home.data.GitHubRepoDao
import com.pingo.gitcatalog.shared.db.AppDatabase
import com.pingo.gitcatalog.shared.models.GithubRepo
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4


/**
 * Created By : Muhammad Ali Ansari
 * Dated :  2019-07-14.
 * ---------------------------------------------
 *
 */
@RunWith(JUnit4::class)
class AppDatabaseTest {

    private var dao: GitHubRepoDao? = null
    private var mDatabase: AppDatabase? = null

    @Before
    @Throws(Exception::class)
    fun setup() {
        mDatabase = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getContext(),
            AppDatabase::class.java
        )
            .allowMainThreadQueries()
            .build()

        dao = mDatabase?.gitHubRepoDao()
    }


    @After
    fun tearDown() {
        mDatabase?.clearAllTables()
        mDatabase?.close()
    }

    @Test
    fun insertGithubReposForTesting() {
        runBlocking {
            val item = getReposTestData()
            dao?.saveRepos(listOf(item))
            val itemTest = dao?.getRepo(item.url)
            Assert.assertEquals(item.url, itemTest?.url)
        }
    }


    @Test
    fun deleteAllGithubRepos() {
        runBlocking {
            dao?.deleteAll()
            Assert.assertEquals(dao?.getRowCount(), 0)
        }

    }


    /** Get github repos test data response just like the API for mocking
     * @return GithubRepo
     */
    private fun getReposTestData(): GithubRepo {
        val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
        val jsonAdapter = moshi.adapter(GithubRepo::class.java)

        return jsonAdapter.fromJson(JSON_DATA_FOR_TESTING)!!

    }

    private val JSON_DATA_FOR_TESTING: String = "{\n" +
            "    \"author\": \"lenve\",\n" +
            "    \"name\": \"vhr\",\n" +
            "    \"avatar\": \"https://github.com/lenve.png\",\n" +
            "    \"url\": \"https://github.com/lenve/vhr\",\n" +
            "    \"description\": \"微人事是一个前后端分离的人力资源管理系统，项目采用SpringBoot+Vue开发。\",\n" +
            "    \"language\": \"Java\",\n" +
            "    \"languageColor\": \"#b07219\",\n" +
            "    \"stars\": 6137,\n" +
            "    \"forks\": 2483,\n" +
            "    \"currentPeriodStars\": 229,\n" +
            "    \"builtBy\": [\n" +
            "      {\n" +
            "        \"username\": \"lenve\",\n" +
            "        \"href\": \"https://github.com/lenve\",\n" +
            "        \"avatar\": \"https://avatars0.githubusercontent.com/u/6023444\"\n" +
            "      }\n" +
            "    ]\n" +
            "  }"
}