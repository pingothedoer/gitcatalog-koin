package com.pingo.gitcatalog.shared.network.api


import com.pingo.gitcatalog.shared.models.GithubRepo
import retrofit2.Response
import retrofit2.http.GET

/**
 * Created By : Muhammad Ali Ansari
 * Dated :  2019-07-13.
 * ---------------------------------------------
 *
 * EndPoint to fetch all gitReposList
 */
interface GithubRepoListService {

    @GET("repositories")
    suspend fun getGithubRepoList(): Response<List<GithubRepo>>
}