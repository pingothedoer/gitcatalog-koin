package com.pingo.gitcatalog.shared.network

import com.pingo.gitcatalog.BuildConfig
import com.pingo.gitcatalog.BuildConfig.BASE_URL
import com.pingo.gitcatalog.shared.network.api.GithubRepoListService
import com.pingo.gitcatalog.shared.network.interceptor.ConnectivityInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by M Ali Ansari
 * on 12/16/2016.
 */

val apiService = module {

    fun getApiService(retrofit: Retrofit) = retrofit.create(GithubRepoListService::class.java)

    single { getApiService(get()) }
}



val retrofitModule = module {

    fun getRetrofit(httpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .client(httpClient)
            .build()
    }

    single {
        getRetrofit(get())
    }



    val connectionTimeOut = 10 * 1000
    val readTimeOut = 10 * 1000
    val writeTimeOut = 60 * 1000

    fun okHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        when {
            BuildConfig.DEBUG -> interceptor.level = HttpLoggingInterceptor.Level.BODY
            else -> interceptor.level = HttpLoggingInterceptor.Level.NONE
        }

        val userHttpClientBuilder = OkHttpClient.Builder()
            .readTimeout(readTimeOut.toLong(), TimeUnit.MILLISECONDS)
            .writeTimeout(writeTimeOut.toLong(), TimeUnit.MILLISECONDS)
            .addInterceptor(interceptor)
            .addInterceptor(ConnectivityInterceptor())
            .connectTimeout(connectionTimeOut.toLong(), TimeUnit.MILLISECONDS)

        return userHttpClientBuilder.build()
    }

    single { okHttpClient() }

}