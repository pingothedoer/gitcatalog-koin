package com.pingo.gitcatalog.shared.network.exception

import java.io.IOException

/**
 * Created by : Muhammad Ali Ansari
 * Dated :  2019-07-13.
 * ----------------------------------------------
 *
 * Custom exception for Internet connections
 */

class InternetException : IOException(){
    override val message: String?
        get() = "Please check your internet connection"
}