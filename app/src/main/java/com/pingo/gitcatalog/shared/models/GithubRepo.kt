package com.pingo.gitcatalog.shared.models


import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

/**
 * Created By : Muhammad Ali Ansari
 * Dated :  2019-07-13.
 * ---------------------------------------------
 *
 * Data Model to parse Github repo data
 */
@Parcelize
@Entity(tableName = "github-repo")
data class GithubRepo(

    @PrimaryKey
    var url: String = "",

    var author: String = "",

    var avatar: String = "",

    var currentPeriodStars: Int = 0,

    var description: String = "",

    var forks: Int = 0,

    var language: String = "",

    var languageColor: String = "",

    var name: String = "",

    var stars: Int = 0,

    @Ignore
    var expanded: Boolean = false
):Parcelable
