package com.pingo.gitcatalog.shared.models

/**
 * Created By : Muhammad Ali Ansari
 * Dated :  2019-07-13.
 * ---------------------------------------------
 */
enum class SortBy {
    NAME,
    STARS
}