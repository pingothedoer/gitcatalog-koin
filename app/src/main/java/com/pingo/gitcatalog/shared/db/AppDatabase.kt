package com.pingo.gitcatalog.shared.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.pingo.gitcatalog.app.home.data.GitHubRepoDao
import com.pingo.gitcatalog.shared.models.GithubRepo

/**
 * Created By : Muhammad Ali Ansari
 * Dated :  2019-07-13.
 * ---------------------------------------------
 *
 * Configures Room Database
 */
@Database(entities = [GithubRepo::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun gitHubRepoDao(): GitHubRepoDao
}