package com.pingo.gitcatalog.app.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.pingo.gitcatalog.R
import com.pingo.gitcatalog.shared.ext.setFullscreen

/**
 * Created By : Muhammad Ali Ansari
 * Dated :  2019-07-13.
 * ---------------------------------------------
 *
 * Activity container for [HomeFragment] to show all trending github repos
 */
class HomeActivity : AppCompatActivity(R.layout.activity_fragment_template) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFullscreen()
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().add(
                R.id.fragment_container,
                HomeFragment.newInstance()
            ).commit()
        }

    }
}
