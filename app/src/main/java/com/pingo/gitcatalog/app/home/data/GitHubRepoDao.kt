package com.pingo.gitcatalog.app.home.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.pingo.gitcatalog.shared.models.GithubRepo
import androidx.lifecycle.LiveData



/**
 * Created By : Muhammad Ali Ansari
 * Dated :  2019-07-13.
 * ---------------------------------------------
 *
 * Interacts with database using this abstract layer
 */
@Dao
interface GitHubRepoDao {

    @Query("SELECT * FROM `github-repo` ORDER BY name COLLATE NOCASE ASC")
    suspend fun getRepoByNames(): List<GithubRepo>


    @Query("SELECT * FROM `github-repo` ORDER BY stars DESC")
    suspend fun getRepoByStars(): List<GithubRepo>


    @Query("SELECT * FROM `github-repo` WHERE url = :id")
    suspend fun getRepo(id: String): GithubRepo

    @Query("SELECT COUNT(url) FROM `github-repo`")
    fun getRowCount(): Int


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    suspend fun saveRepos(user: List<GithubRepo>)


    @Query("DELETE FROM `github-repo`")
    suspend fun deleteAll()

}