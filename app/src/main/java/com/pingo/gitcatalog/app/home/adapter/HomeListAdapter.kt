package com.pingo.gitcatalog.app.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pingo.gitcatalog.R
import com.pingo.gitcatalog.shared.ext.showImage
import com.pingo.gitcatalog.shared.ext.tintDrawable
import com.pingo.gitcatalog.shared.models.GithubRepo
import kotlinx.android.synthetic.main.item_github_repo_list.view.*


/**
 * Created By : Muhammad Ali Ansari
 * Dated :  2019-07-13.
 * ----------------------------------------------
 *
 * [RecyclerView.Adapter] displaying git repos catalog.
 */
class HomeListAdapter : RecyclerView.Adapter<HomeListAdapter.GithubRepoViewHolder>() {

    private var results = ArrayList<GithubRepo>()
    private var preExpPosition = -1

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): GithubRepoViewHolder {
        return GithubRepoViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(
                R.layout.item_github_repo_list, viewGroup, false
            )
        )
    }

    /**
     * Binds data with view holder
     * @param viewHolder GithubRepoViewHolder
     * @param position Int
     */
    override fun onBindViewHolder(viewHolder: GithubRepoViewHolder, position: Int) {

        val repo = results[viewHolder.adapterPosition]

        viewHolder.bind(repo)

        viewHolder.itemView.setOnClickListener {

            // Close previous one first
            if (preExpPosition != -1) {
                val preExpRepo = results[preExpPosition]
                val expandedState = preExpRepo.expanded
                preExpRepo.expanded = !expandedState
                notifyItemChanged(preExpPosition)
            }

            if (preExpPosition != viewHolder.adapterPosition) {
                // Now expand other
                val expandedState = repo.expanded
                repo.expanded = !expandedState
                preExpPosition = viewHolder.adapterPosition
                notifyItemChanged(position)
            } else {
                preExpPosition = -1
            }
        }
    }

    /**
     * Item Count
     * @return Int
     */
    override fun getItemCount() = results.size


    /**
     * Appends existing results with the new ones
     * @param results List<GithubRepo>
     */
    fun updateResults(results: List<GithubRepo>) {
        preExpPosition = -1
        this.results.clear()
        this.results.addAll(results)
        notifyDataSetChanged()
    }


    /**
     * View holder
     * @property view View
     * @constructor
     */
    inner class GithubRepoViewHolder(private var view: View) : RecyclerView.ViewHolder(view) {

        // bind data with views
        fun bind(repo: GithubRepo) {

            view.tvDevName.text = repo.author
            view.tvRepoTitle.text = repo.name
            view.tvDetail.text = repo.description
            view.stars.text = repo.stars.toString()
            view.fork.text = repo.forks.toString()
            view.source.text = repo.language
            view.source.tintDrawable(repo.languageColor)
            view.avatar.showImage(repo.avatar)

            when {
                repo.expanded -> view.detailContainer.visibility = View.VISIBLE
                else -> view.detailContainer.visibility = View.GONE
            }


        }
    }
}