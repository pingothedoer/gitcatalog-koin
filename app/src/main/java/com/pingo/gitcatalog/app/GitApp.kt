package com.pingo.gitcatalog.app

import android.app.Application
import androidx.room.Room
import com.pingo.gitcatalog.app.home.data.homeRepo
import com.pingo.gitcatalog.app.home.data.homeViewModel
import com.pingo.gitcatalog.shared.db.AppDatabase
import com.pingo.gitcatalog.shared.network.apiService
import com.pingo.gitcatalog.shared.network.retrofitModule
import org.koin.core.context.startKoin

/**
 * Created By : Muhammad Ali Ansari
 * Dated :  2019-10-01.
 * ---------------------------------------------
 */
class GitApp : Application() {

    var database: AppDatabase? = null
        private set

    override fun onCreate() {
        super.onCreate()

        // Init Room Database
        database = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java,
            "gitkoin"
        ).build()


        startKoin {
            modules(listOf(homeViewModel, homeRepo, retrofitModule , apiService))
        }

        app = this
    }

    companion object {

        var app: GitApp? = null
            private set
    }

}
