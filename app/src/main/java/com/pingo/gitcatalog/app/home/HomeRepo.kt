package com.pingo.gitcatalog.app.home

import com.pingo.gitcatalog.shared.models.GithubRepo
import com.pingo.gitcatalog.shared.network.api.GithubRepoListService
import org.koin.android.ext.android.inject
import retrofit2.Response

/**
 * Created By : Muhammad Ali Ansari
 * Dated :  2019-10-16.
 * ---------------------------------------------
 */
class HomeRepo(private val service: GithubRepoListService) {

    /**
     * Fetch online repos
     * @return Response<List<GithubRepo>>
     */
    suspend fun fetchOnlineRepos(): Response<List<GithubRepo>> {
        return service.getGithubRepoList()
    }
}