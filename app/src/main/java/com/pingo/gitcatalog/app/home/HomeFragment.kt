package com.pingo.gitcatalog.app.home

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import com.pingo.gitcatalog.R
import com.pingo.gitcatalog.app.home.adapter.HomeListAdapter
import com.pingo.gitcatalog.databinding.FragmentHomeBinding
import com.pingo.gitcatalog.shared.ext.setup
import com.pingo.gitcatalog.shared.models.SortBy
import com.pingo.gitcatalog.shared.ui.BaseFragment
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.error_layout.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.loading_layout.*
import org.koin.androidx.viewmodel.ext.android.getViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


/**
 * Created By : Muhammad Ali Ansari
 * Dated :  2019-07-13.
 * ---------------------------------------------
 *
 * Showing List of all github repos
 */
class HomeFragment : BaseFragment<FragmentHomeBinding>(R.layout.fragment_home) {


    private var homeListAdapter: HomeListAdapter? = null
    private val viewModelObj by viewModel<HomeViewModel>()

    companion object {
        fun newInstance() = HomeFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        homeListAdapter = HomeListAdapter()
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        // set data binding
        binding.apply {
            viewModel = viewModelObj
            lifecycleOwner = this@HomeFragment

        }

        // init all view
        initViews()

        // register observers
        initObservers()

        // fetch git repo list  as we have started listening changes
        viewModelObj.fetchGitRepos()

    }


    /**
     * Initializing All Views
     */
    private fun initViews() {

        // set recycler view
        with(rvGitRepo.setup()) {
            adapter = homeListAdapter
        }

        // set pull to refresh
        refreshLayout.setOnRefreshListener {
            viewModelObj.fetchOnlineGitRepos()
        }

    }


    /**
     * Start listening [androidx.lifecycle.Observer]
     */
    private fun initObservers() {

        // repo list
        viewModelObj.gitReposList.observe(this, Observer {

            if (it.isNullOrEmpty()) {
                errorLayout.visibility = View.VISIBLE
                homeListAdapter?.updateResults(emptyList())
            } else {
                homeListAdapter?.updateResults(it)
            }
        })


        // listen for connection error
        viewModelObj.connectionError.observe(this, Observer {
            errorLayout.visibility = View.VISIBLE
            tvErrorDetail.text = getString(R.string.connection_error_msg)
        })


        // listen for  error
        viewModelObj.errorMessage.observe(this, Observer {
            errorLayout.visibility = View.VISIBLE
            tvErrorDetail.text = it
        })


        viewModelObj.showProgress.observe(this, Observer {
            if (it) {
                errorLayout.visibility = View.GONE
                if (loader != null) {
                    loader.inflate()
                    shimmerLayout.startShimmer()
                } else {
                    homeListAdapter?.updateResults(emptyList())
                    refreshLayout.isRefreshing = true
                }
            } else {
                refreshLayout.isRefreshing = false
                shimmerLayout?.stopShimmer()
                shimmerLayout?.visibility = View.GONE
            }
        })

    }


    /******************** OPTIONS MENU <START> ******************/

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_sort_star -> {
                viewModelObj.fetchGitRepos(sortBy = SortBy.STARS)
                true
            }
            R.id.action_sort_name -> {
                viewModelObj.fetchGitRepos(sortBy = SortBy.NAME)
                true
            }
            else -> false
        }
    }

    /******************** OPTIONS MENU <END> ******************/
}
