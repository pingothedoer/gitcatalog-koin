package com.pingo.gitcatalog.app.home.data

import com.pingo.gitcatalog.app.home.HomeRepo
import com.pingo.gitcatalog.app.home.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created By : Muhammad Ali Ansari
 * Dated :  2019-10-16.
 * ---------------------------------------------
 */

val homeViewModel = module {
    viewModel { HomeViewModel(get()) }
}

val homeRepo = module {
    single { HomeRepo(get()) }
}