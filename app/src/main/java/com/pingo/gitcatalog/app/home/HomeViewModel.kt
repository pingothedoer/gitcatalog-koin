package com.pingo.gitcatalog.app.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.pingo.gitcatalog.app.GitApp
import com.pingo.gitcatalog.shared.models.GithubRepo
import com.pingo.gitcatalog.shared.models.SortBy
import com.pingo.gitcatalog.shared.ui.BaseViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*


/**
 * Created by : Muhammad Ali Ansari
 * Dated :  2019-07-13.
 * ----------------------------------------------
 *
 * Bridging [HomeFragment] and Data Source
 */
open class HomeViewModel(private val homeRepo: HomeRepo) : BaseViewModel() {

    private var selectedSortOrder = SortBy.NAME
    val gitReposList: MutableLiveData<List<GithubRepo>> = MutableLiveData()


    /**
     * Fetching gitReposList catalog from online storage
     */
    fun fetchOnlineGitRepos() {

        viewModelScope.launch {
            try {
                showProgress.postValue(true)
                val response = homeRepo.fetchOnlineRepos()
                if (response.isSuccessful) {
                    GitApp.app?.database?.gitHubRepoDao()
                        ?.saveRepos(response.body() as ArrayList<GithubRepo>)
                    fetchGitRepos()
                } else {
                    onBaseError(response.errorBody())
                }

            } catch (exp: Exception) {
                onBaseError(exp)
            }
        }
    }


    /**
     * Fetching gitReposList catalog from local storage
     */
    fun fetchGitRepos(sortBy: SortBy = SortBy.NAME) {

        showProgress.postValue(true)

        viewModelScope.launch {
            delay(2000)
            try {
                selectedSortOrder = sortBy
                val repoList = getLocalGitRepos(selectedSortOrder)
                showProgress.postValue(false)
                gitReposList.postValue(repoList)
            } catch (exp: Exception) {
                onBaseError(exp)
            }
        }
    }


    /**
     * Fetch trending gitReposList
     * @return List<GithubRepo>
     */
    private suspend fun getLocalGitRepos(sortBy: SortBy): List<GithubRepo>? {
        return when (sortBy) {
            SortBy.STARS -> GitApp.app?.database?.gitHubRepoDao()?.getRepoByStars()
            SortBy.NAME -> GitApp.app?.database?.gitHubRepoDao()?.getRepoByNames()
        }
    }
}